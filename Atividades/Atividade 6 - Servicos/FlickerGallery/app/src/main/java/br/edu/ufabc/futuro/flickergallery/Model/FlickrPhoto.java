package br.edu.ufabc.futuro.flickergallery.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ufabc on 01/04/15.
 */
public class FlickrPhoto {
    //POJO

    private Long id;
    private String title;
    private String thumbURL;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbURL() {
        return thumbURL;
    }

    public void setThumbURL(String thumbURL) {
        this.thumbURL = thumbURL;
    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeLong(id);
//        dest.writeString(title);
//        dest.writeString(thumbURL);
//    }
}

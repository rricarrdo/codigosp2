package br.edu.ufabc.futuro.flickergallery.View;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

import br.edu.ufabc.futuro.flickergallery.Adapter.FlickrPhotoAdapter;
import br.edu.ufabc.futuro.flickergallery.Controller.FlickrJSONFetcher_Service;
import br.edu.ufabc.futuro.flickergallery.Model.FlickrPhoto;
import br.edu.ufabc.futuro.flickergallery.R;


public class FlickrGalleryActivity extends ActionBarActivity {

    private GridView gridView;
    private DownloadReceiver receiver;
    private static final String LOGTAG = FlickrGalleryActivity.class.getSimpleName();

//    private FlickrFetchTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flicker_gallery);

        gridView = (GridView) findViewById(R.id.photoGrid);
//        task = new FlickrFetchTask();

        if (isNetworkConnected()) {
//            task.execute(new FlickrFetcher());

            Intent intent = new Intent(this, FlickrJSONFetcher_Service.class);
            intent.putExtra(FlickrJSONFetcher_Service.PARAMS_CACHE_DIR, this.getCacheDir().getPath());

            startService(intent);
        }

        registerBroadcast();
    }

    private void registerBroadcast() {
        IntentFilter downloadCompleteFilter = new IntentFilter(FlickrJSONFetcher_Service.BROADCAST_ACTION);

        receiver = new DownloadReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, downloadCompleteFilter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_flicker_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reload) {
            Toast.makeText(this, "Reloading...", Toast.LENGTH_LONG).show();
            gridView.invalidateViews();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupAdapter(ArrayList<FlickrPhoto> photos) {
        gridView.setAdapter(new FlickrPhotoAdapter(this, photos));
    }

//    private class FlickrFetchTask extends AsyncTask<FlickrFetcher, Void, ArrayList<FlickrPhoto>>
//    {
//        @Override
//        protected ArrayList<FlickrPhoto> doInBackground(FlickrFetcher... params) {
//            return params[0].fetch();
//        }
//
//        @Override
//        protected void onPostExecute(ArrayList<FlickrPhoto> flickrPhotos) {
//            setupAdapter(flickrPhotos);
//        }
//    }


    public boolean isNetworkConnected() {
        boolean status = false;
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected())
            status = true;

        return status;
    }

    private class DownloadReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(FlickrJSONFetcher_Service.BROADCAST_ACTION)) {
                ArrayList<FlickrPhoto> flickrPhotos = (ArrayList<FlickrPhoto>) intent.getSerializableExtra(FlickrJSONFetcher_Service.PARAMS_FLICKR_PHOTOS);

                if (flickrPhotos != null) {
//                    Toast.makeText(context, "Funcionouuu", Toast.LENGTH_LONG).show();
                    setupAdapter(flickrPhotos);
                } else {
//                    Toast.makeText(context, "Errrrrrroou!", Toast.LENGTH_LONG).show();
                    Log.e(LOGTAG, "Não foi obtido nenhum retorno do serviço de download do JSON");
                }
            }
        }
    }
}


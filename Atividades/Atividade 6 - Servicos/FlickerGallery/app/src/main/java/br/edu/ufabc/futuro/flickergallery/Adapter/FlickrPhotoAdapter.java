package br.edu.ufabc.futuro.flickergallery.Adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import br.edu.ufabc.futuro.flickergallery.Controller.FlickrThumbDownload;
import br.edu.ufabc.futuro.flickergallery.Model.FlickrPhoto;
import br.edu.ufabc.futuro.flickergallery.R;

/**
 * Created by ufabc on 01/04/15.
 */
public class FlickrPhotoAdapter extends BaseAdapter {

    private ArrayList<FlickrPhoto> photos;
    private Context context;
    private LayoutInflater inflater;

    private static final String LOGTAG = FlickrPhotoAdapter.class.getSimpleName();

    public FlickrPhotoAdapter(Context context, ArrayList<FlickrPhoto> photos) {
        this.photos = photos;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public Object getItem(int position) {
        return photos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return photos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView ivThumb;
        FlickrPhoto flickrPhoto = photos.get(position);

//        if(convertView == null){
        convertView = inflater.inflate(R.layout.grid_item, null);
//        }


        ivThumb = (ImageView) convertView.findViewById(R.id.thumbnail);
        ivThumb.setImageBitmap(null);

        FlickrThumbDownload downloadThumbTask = new FlickrThumbDownload(flickrPhoto.getThumbURL(), ivThumb, context.getCacheDir().getPath());
        downloadThumbTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return convertView;
    }

}

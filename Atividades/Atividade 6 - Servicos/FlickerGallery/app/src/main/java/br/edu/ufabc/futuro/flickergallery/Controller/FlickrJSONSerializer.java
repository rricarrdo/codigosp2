package br.edu.ufabc.futuro.flickergallery.Controller;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.edu.ufabc.futuro.flickergallery.Model.FlickrPhoto;

/**
 * Created by ufabc on 01/04/15.
 */
public class FlickrJSONSerializer {

    private static final String LOGTAG = FlickrJSONSerializer.class.getSimpleName();

    //JSON names
    private static final String PHOTO_OBJECT = "photos";
    private static final String PHOTO_ARRAY = "photo";
    private static final String ID = "id";
    private static final String TITLE = "title";
    private static final String THUMB_URL = "url_s";

    public static ArrayList<FlickrPhoto> deserialize(String jsonStr, String cachePath) {
        ArrayList<FlickrPhoto> photos = new ArrayList<>();
        JSONObject root;
        String thumbURL;
        FlickrThumbDownload downloadThumbTask;

        try {
            JSONArray photosArray;
            root = new JSONObject(jsonStr);
            photosArray = root.getJSONObject(PHOTO_OBJECT).getJSONArray(PHOTO_ARRAY);
            FlickrPhoto photo;
            JSONObject photoObj;

            for (int i = 0; i < photosArray.length(); i++) {
                photo = new FlickrPhoto();
                photoObj = (JSONObject) photosArray.get(i);

                photo.setId(Long.parseLong(photoObj.getString(ID)));
                photo.setTitle(photoObj.getString(TITLE));
                thumbURL = photoObj.getString(THUMB_URL);
                photo.setThumbURL(thumbURL);

                downloadThumbTask = new FlickrThumbDownload(thumbURL, null, cachePath);
                downloadThumbTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                photos.add(photo);
            }
        } catch (JSONException e) {
            Log.e(LOGTAG, "Failed to parse JSON", e);
        }

        return photos;
    }
}

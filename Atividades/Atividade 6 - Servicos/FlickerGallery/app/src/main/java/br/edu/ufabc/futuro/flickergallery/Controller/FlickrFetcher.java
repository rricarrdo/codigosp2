package br.edu.ufabc.futuro.flickergallery.Controller;

import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import br.edu.ufabc.futuro.flickergallery.Model.FlickrPhoto;

/**
 * Created by ufabc on 01/04/15.
 */
public class FlickrFetcher {
    private static final String ENDPOINT = "https://api.flickr.com/services/rest/";

    private static final String API_KEY_K = "api_key";
    private static final String FORMAT_K = "format";
    private static final String EXTRAS_K = "extras";
    private static final String METHOD_K = "method";
    private static final String NO_JSON_CALLBACK_K = "nojsoncallback";
//Segredo: 0099e053699adc12
    private static final String API_KEY_V = "85b235a76512e4100c8d16b34e0a26ca";
    private static final String FORMAT_V = "json";
    private static final String EXTRAS_V = "url_s";
    private static final String METHOD_V = "flickr.photos.getRecent";
    private static final String NO_JSON_CALLBACK_V = "1";

    private static final String LOGTAG = FlickrFetcher.class.getSimpleName();
    private static final int BUFFER_SIZE = 1024;


    public ArrayList<FlickrPhoto> fetch(String cachePath){
        ArrayList<FlickrPhoto> photos = new ArrayList<>();
        URL url;
        String urlStr = null;
        HttpURLConnection conn = null;
        int responseCode;

        try{
            urlStr = Uri.parse(ENDPOINT).buildUpon()
                    .appendQueryParameter(API_KEY_K, API_KEY_V)
                    .appendQueryParameter(FORMAT_K, FORMAT_V)
                    .appendQueryParameter(EXTRAS_K, EXTRAS_V)
                    .appendQueryParameter(METHOD_K, METHOD_V)
                    .appendQueryParameter(NO_JSON_CALLBACK_K, NO_JSON_CALLBACK_V)
                    .build().toString();


            url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            responseCode = conn.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK){
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                InputStream inputStream = conn.getInputStream();
                int len;
                byte[] buffer = new byte[BUFFER_SIZE];

                while((len = inputStream.read(buffer)) > 0){
                    outputStream.write(buffer, 0, len);
                }

                outputStream.flush();
                outputStream.close();
                inputStream.close();

                // deserialize JSON
                String jsonStr = new String(outputStream.toByteArray());
                photos.addAll(FlickrJSONSerializer.deserialize(jsonStr, cachePath));
            }

        }catch (MalformedURLException e) {
            Log.e(LOGTAG, "Malformed URL", e);
        }catch (IOException e) {
            Log.e(LOGTAG, "Failed to open URL", e);
        }

        return photos;
    }

}

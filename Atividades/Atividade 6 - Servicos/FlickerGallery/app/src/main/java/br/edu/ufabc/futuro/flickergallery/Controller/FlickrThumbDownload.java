package br.edu.ufabc.futuro.flickergallery.Controller;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Ricardo on 16/04/15.
 */


public class FlickrThumbDownload extends AsyncTask<Void, Void, Bitmap> {

    private static final String LOGTAG = FlickrThumbDownload.class.getSimpleName();
    private String urlStr;
    private String cachePath;
    private ImageView imageView;

    public FlickrThumbDownload(String urlStr, ImageView imageView, String cachePath) {
        this.urlStr = urlStr;
        this.imageView = imageView;
        this.cachePath = cachePath;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {

        URL url;
        HttpURLConnection connection;
        int responseCode = 0;
        Bitmap bitmap = null;

        File cacheDir = new File(cachePath);
        cacheDir.mkdir();
        String fileName = urlStr.substring(urlStr.lastIndexOf('/') + 1);
        File photoCache = new File(cacheDir + File.separator + fileName);

        bitmap = BitmapFactory.decodeFile(photoCache.getAbsolutePath());

        if (bitmap != null) {
            Log.i(LOGTAG, "Leu do Cache!");
            return bitmap;
        }

        try {
            url = new URL(urlStr);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {

                Log.w(LOGTAG, "Downloading item " + urlStr);

                bitmap = BitmapFactory.decodeStream(connection.getInputStream());

                FileOutputStream fOut = new FileOutputStream(photoCache);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fOut);
                fOut.flush();
                fOut.close();
                Log.i(LOGTAG, "Salvou no Cache!!!");
            } else {
                Log.e(LOGTAG, "Failed to dowload bitmap");
            }
        } catch (MalformedURLException e) {
            Log.e(LOGTAG, "Malformed URL", e);
        } catch (IOException e) {
            Log.e(LOGTAG, "Failed to open URL: " + urlStr, e);
        }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {

        if (imageView != null &&
                bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
    }
}

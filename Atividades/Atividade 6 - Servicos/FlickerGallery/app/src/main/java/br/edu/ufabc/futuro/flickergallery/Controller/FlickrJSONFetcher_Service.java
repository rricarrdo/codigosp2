package br.edu.ufabc.futuro.flickergallery.Controller;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;

import br.edu.ufabc.futuro.flickergallery.Model.FlickrPhoto;


public class FlickrJSONFetcher_Service extends IntentService {
    public static final String PARAMS_CACHE_DIR = "cacheDir";
    public static final String PARAMS_FLICKR_PHOTOS = "flickrPhotos";
    public static final String BROADCAST_ACTION = FlickrJSONFetcher_Service.class.getPackage() + ".DOWNLOADCOMPLETE";

    private FlickrFetcher flickrFetcher;

    public FlickrJSONFetcher_Service() {
        super("FlickrJSONFetcher_Service");
        flickrFetcher = new FlickrFetcher();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String cachePath = intent.getStringExtra(PARAMS_CACHE_DIR);
        ArrayList<FlickrPhoto> flickrPhotos = flickrFetcher.fetch(cachePath);

        Intent localIntent;

        localIntent = new Intent(BROADCAST_ACTION);
        localIntent.putExtra(PARAMS_FLICKR_PHOTOS, flickrPhotos);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }
}

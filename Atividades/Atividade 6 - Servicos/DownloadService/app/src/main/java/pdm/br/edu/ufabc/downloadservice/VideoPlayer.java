package pdm.br.edu.ufabc.downloadservice;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;


public class VideoPlayer extends ActionBarActivity {

    private static final String LOGTAG = VideoPlayer.class.getSimpleName();

    public static final String VIDEO_URI_PARAM = "videoURI";

    VideoView videoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent;
        File file;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        intent = getIntent();
        file = (File) intent.getSerializableExtra(VIDEO_URI_PARAM);

        if (file != null) {
            videoPlayer = (VideoView) findViewById(R.id.video_player);
            MediaController mediaController = new MediaController(this);
            mediaController.setAnchorView(videoPlayer);
            videoPlayer.setMediaController(mediaController);

            videoPlayer.setVideoURI(Uri.fromFile(file));
            videoPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    videoPlayer.start();
                }
            });
        } else
            Toast.makeText(this, getString(R.string.video_play_failed),
                    Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_video_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

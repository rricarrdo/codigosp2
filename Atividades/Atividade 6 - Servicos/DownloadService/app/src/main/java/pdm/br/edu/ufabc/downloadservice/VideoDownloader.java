package pdm.br.edu.ufabc.downloadservice;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by diogo on 06/04/15.
 */
public class VideoDownloader {

    private static final int BUFFER_SIZE = 4 * 1024;
    private static final int PROGRESS_STEP_COUNT = 20;
    private static final String LOGTAG = VideoDownloader.class.getSimpleName();
    public static final String BROADCAST_DOWNLOAD_PROGRESS = VideoDownloader.class.getPackage() + ".DOWNLOAD_PROGRESS";
    public static final String DOWNLOAD_PROGRESS_KEY = "download_progress";
    public static final String DOWNLOAD_SIZE_KEY = "download_size";

    private int contentLength;
    private Context context;

    public VideoDownloader(Context context) {
        this.context = context;
    }

    protected File download(String urlStr, String dir) {
        URL url;
        HttpURLConnection conn;
        String fileName;
        File file = null;
        int responseCode;

        if (isNetworkConnected()) {
            try {
                url = new URL(urlStr);
                fileName = (new File(url.getPath())).getName();
                conn = (HttpURLConnection )url.openConnection();
                conn.connect(); // open connection
                responseCode = conn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    contentLength = conn.getContentLength();
                    // start download on external storage
                    file = saveToExternalStorage(conn.getInputStream(), dir, fileName);
                    conn.disconnect(); // close connection
                } else
                    Log.e(LOGTAG, "Failed to download resource: HTTP code " + responseCode);
            } catch(MalformedURLException e) {
                Log.e(LOGTAG, "Malformed URL", e);
            } catch (IOException e) {
                Log.e(LOGTAG, "Failed to open a connection", e);
            }
        } else
            Log.e(LOGTAG, "No network connection is available");

        return file;
    }

    /**
     * Continue downloading the video, saving it directly to external storage
     *
     * @param is the download input stream
     * @param dir the directory in which the video will be saved
     * @param fileName the filename of the video
     * @return true if the video could be saved, false otherwise
     */
    private File saveToExternalStorage(InputStream is, String dir, String fileName) {
        File file = null;

        if (isExternalStorageWritable()) {
            File parent = new File(dir);

            file = new File(dir, fileName);
            if (parent.isDirectory() || parent.mkdirs()) {
                try {
                    BufferedInputStream inStream = new BufferedInputStream(is, BUFFER_SIZE);
                    FileOutputStream outStream = new FileOutputStream(file);
                    byte[] buff = new byte[BUFFER_SIZE];
                    int len;
                    int read = 0;
                    int notifyBuff = 0;
                    int stepSize = contentLength / PROGRESS_STEP_COUNT;

                    while ((len = inStream.read(buff)) != -1) {
                        outStream.write(buff, 0, len);
                        read += len;
                        notifyBuff += len;
                        if (notifyBuff >= stepSize) {
                            // notify in steps, to avoid overflowing the
                            // broadcast bus
                            notifyProgress(read);
                            notifyBuff = 0;
                        }
                    }
                    notifyProgress(read);
                    outStream.flush();
                    outStream.close();
                    inStream.close();
                } catch (FileNotFoundException e) {
                    Log.e(LOGTAG, "Failed to create external file", e);
                } catch (IOException e) {
                    Log.e(LOGTAG, "Failed to write to external file", e);
                }
            } else {
                Log.e(LOGTAG, "Failed to create external file path: " + parent);
            }
        }

        return file;
    }

    /**
     * Send a broadcast intent to notify the progress of the download
     * @param progress the percent of progress
     */
    private void notifyProgress(int progress) {
        Intent intent = new Intent(BROADCAST_DOWNLOAD_PROGRESS);
        
        intent.putExtra(DOWNLOAD_PROGRESS_KEY, progress);
        intent.putExtra(DOWNLOAD_SIZE_KEY, contentLength);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    /**
     * Check if there is a working network connection in the device
     *
     * @return true if there is a connection, false otherwise
     */
    public boolean isNetworkConnected() {
        boolean status = false;

        ConnectivityManager connectivityManager =
                (ConnectivityManager )context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected())
            status = true;

        return status;
    }

    /**
     * Check if external storage has read permission for the app. Should be called at
     * every attempt to read the external storage
     * @return true in case it is readable, false otherwise
     */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * Check if the external storage has write permission for this app. Should be called at
     * every attempt to write to external storage
     *
     * @return true if it is writable, false otherwise
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
}

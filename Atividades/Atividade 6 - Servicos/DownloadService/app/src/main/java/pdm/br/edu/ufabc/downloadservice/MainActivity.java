package pdm.br.edu.ufabc.downloadservice;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import java.io.File;


public class MainActivity extends ActionBarActivity {

    private static final String LOGTAG = MainActivity.class.getSimpleName();
    private Button downloadButton;
    private ProgressBar download_progress;
    private DownloadReceiver receiver;

    //    private static final String VIDEO_URL = "http://video.webmfiles.org/big-buck-bunny_trailer.webm"; // ~2mb
    private static final String VIDEO_URL = "https://dl.dropboxusercontent.com/u/1184232/trailer_1080p.mp4"; // ~20MB

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        download_progress = (ProgressBar) findViewById(R.id.download_progress);

        downloadButton = (Button) findViewById(R.id.download_button);
        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), VideoDownloadService.class);

                intent.putExtra(VideoDownloadService.PARAMS_VIDEO_URL, VIDEO_URL);
                startService(intent);
                downloadButton.setEnabled(false);
            }
        });

        registerBroadcast();
    }

    private void registerBroadcast() {
        IntentFilter downloadCompleteFilter = new IntentFilter(VideoDownloadService.BROADCAST_ACTION);
        IntentFilter downloadProgressFilter = new IntentFilter(VideoDownloader.BROADCAST_DOWNLOAD_PROGRESS);

        receiver = new DownloadReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, downloadCompleteFilter);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, downloadProgressFilter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DownloadReceiver extends BroadcastReceiver {

        private NotificationManager manager;

        public DownloadReceiver() {
            manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(VideoDownloadService.BROADCAST_ACTION)) {
                File file = (File) intent.getSerializableExtra("videoFile");

                if (file != null)
                    notifyCompleted(context, file);
                else
                    notifyDownloadError(context);
            } else if (intent.getAction().equals(VideoDownloader.BROADCAST_DOWNLOAD_PROGRESS)) {
                int progress = intent.getIntExtra(VideoDownloader.DOWNLOAD_PROGRESS_KEY, -1);
                int size = intent.getIntExtra(VideoDownloader.DOWNLOAD_SIZE_KEY, -1);

                if (progress >= 0) {
                    notifyProgress(context, progress, size);
                    download_progress.setVisibility(View.VISIBLE);
                    download_progress.setMax(size);
                    download_progress.setProgress(progress);
                } else {
                    notifyDownloadError(context);
                }
            }

        }

        private void notifyCompleted(Context context, File file) {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
            Intent notificationIntent = new Intent(context, VideoPlayer.class);
            PendingIntent pendingIntent;
            Notification notification;

            // build the notification
            mBuilder.setSmallIcon(android.R.drawable.stat_sys_download_done)
                    .setContentTitle("Video downloader")
                    .setContentText("Download has finished!");
            // configure the intent and the pending intent
            notificationIntent.putExtra(VideoPlayer.VIDEO_URI_PARAM, file);
            pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pendingIntent);
            // dismiss the notification after click
            notification = mBuilder.build();
            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            // trigger the notification
            manager.notify(0, notification);
        }

        private void notifyDownloadError(Context context) {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(android.R.drawable.stat_notify_error)
                            .setContentTitle("Video downloader")
                            .setContentText("Something went wrong with the downloaded file!");

            manager.notify(0, mBuilder.build());
        }

        private void notifyProgress(Context context, int progress, int size) {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(android.R.drawable.stat_notify_sync)
                            .setContentTitle("Video downloader")
                            .setContentText("Downloading file");
            mBuilder.setProgress(
                    size, // max
                    progress, // progress
                    false // indeterminate?
            );

            manager.notify(0, mBuilder.build());
        }
    }
}

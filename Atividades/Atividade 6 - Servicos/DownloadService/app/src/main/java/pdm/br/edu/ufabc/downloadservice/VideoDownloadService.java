package pdm.br.edu.ufabc.downloadservice;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class VideoDownloadService extends IntentService {

    private static final String LOGTAG = VideoDownloadService.class.getSimpleName();
    private static final String VIDEO_DIR =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).toString();

    public static final String PARAMS_VIDEO_URL = "videoURL";
    public static final String BROADCAST_ACTION = VideoDownloadService.class.getPackage() + ".DOWNLOADCOMPLETE";

    private VideoDownloader downloader;

    public VideoDownloadService() {
        super("VideoDownloadService");

        downloader = new VideoDownloader(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String urlStr = intent.getStringExtra(PARAMS_VIDEO_URL);
        Intent localIntent;

        File file = downloader.download(urlStr, VIDEO_DIR);

        localIntent = new Intent(BROADCAST_ACTION);
        localIntent.putExtra("videoFile", file);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }
}

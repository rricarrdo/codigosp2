package br.edu.ufabc.futuro.atividade5_geonames.Controller;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.edu.ufabc.futuro.atividade5_geonames.Model.GeoName;

/**
 * Created by Ricardo on 06/04/2015.
 */
public class GeoName_Serializer {

    private static final String LOGTAG = GeoName_Serializer.class.getSimpleName();

    private static final String GEONAME_ARRAY = "geonames";

    private static final String CODE_NAME = "fcodeName";
    private static final String TOPONYM_NAME = "toponymName";
    private static final String COUNTRY_CODE = "countrycode";
    private static final String FCL = "fcl";
    private static final String FCL_NAME = "fclName";
    private static final String NAME = "name";
    private static final String WIKIPEDIA = "wikipedia";
    private static final String LNG = "lng";
    private static final String GEONAME_ID = "geonameId";
    private static final String LAT = "lat";
    private static final String POPULATION = "population";

    public static ArrayList<GeoName> deserialize(String jsonStr){
        ArrayList<GeoName> geoNames = new ArrayList<>();
        JSONObject root;

        try {
            JSONArray photosArray;
            root = new JSONObject(jsonStr);

            photosArray = root.getJSONArray(GEONAME_ARRAY);
            for (int i = 0; i < photosArray.length(); i++){
                GeoName geoName = new GeoName();
                JSONObject geoObj = (JSONObject) photosArray.get(i);

                geoName.setFcodeName(geoObj.getString(CODE_NAME));
                geoName.setToponymName(geoObj.getString(TOPONYM_NAME));
                geoName.setCountrycode(geoObj.getString(COUNTRY_CODE));
                geoName.setFcl(geoObj.getString(FCL));
                geoName.setFclName(geoObj.getString(FCL_NAME));
                geoName.setName(geoObj.getString(NAME));
                geoName.setWikipedia(geoObj.getString(WIKIPEDIA));
                geoName.setLng(geoObj.getString(LNG));
                geoName.setGeonameId(Long.parseLong(geoObj.getString(GEONAME_ID)));
                geoName.setLat(geoObj.getString(LAT));
                geoName.setPopulation(geoObj.getString(POPULATION));

                geoNames.add(geoName);
            }
        } catch (JSONException e) {
            Log.e(LOGTAG, "Failed to parse JSON", e);
        }

        return geoNames;
    }

}

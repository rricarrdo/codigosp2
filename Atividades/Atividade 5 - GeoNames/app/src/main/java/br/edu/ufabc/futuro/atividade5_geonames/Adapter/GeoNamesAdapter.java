package br.edu.ufabc.futuro.atividade5_geonames.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.ufabc.futuro.atividade5_geonames.Model.GeoName;
import br.edu.ufabc.futuro.atividade5_geonames.Model.GeoNamesDAO;
import br.edu.ufabc.futuro.atividade5_geonames.R;

/**
 * Created by Ricardo on 06/04/2015.
 */
public class GeoNamesAdapter extends BaseAdapter {

    private GeoNamesDAO dao;
    private Context context;
    private LayoutInflater inflater;
    private static final String LOGTAG = GeoNamesAdapter.class.getSimpleName();

    public GeoNamesAdapter(Context context) {
        dao = GeoNamesDAO.newInstance(context);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dao.size();
    }

    @Override
    public Object getItem(int position) {
        return dao.getGeoNameAt(position);
    }

    @Override
    public long getItemId(int position) {
        return dao.geoNames.get(position).getGeonameId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tvNomeCidade;
        GeoName geoName = dao.getGeoNameAt(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item, null);
        }

        tvNomeCidade = (TextView) convertView.findViewById(R.id.tvNomeCidade);
        tvNomeCidade.setText(geoName.getName());

        return convertView;
    }
}

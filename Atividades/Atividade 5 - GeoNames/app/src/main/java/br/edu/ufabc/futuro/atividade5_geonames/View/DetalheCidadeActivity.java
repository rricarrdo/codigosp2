package br.edu.ufabc.futuro.atividade5_geonames.View;


import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import br.edu.ufabc.futuro.atividade5_geonames.Model.GeoName;
import br.edu.ufabc.futuro.atividade5_geonames.Model.GeoNamesDAO;
import br.edu.ufabc.futuro.atividade5_geonames.R;

/*
 * MainActivity class that loads MainFragment
 */
public class DetalheCidadeActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_cidade);

        preencherDetalhes();
    }

    private void preencherDetalhes() {
        int position = getIntent().getIntExtra(ResultadoCidadesActivity.POSITION_GEONAME, 0);

        GeoNamesDAO dao = GeoNamesDAO.newInstance(this);

        GeoName geoName = dao.getGeoNameAt(position);

        TextView tvNome = (TextView) findViewById(R.id.tvNome);
        TextView tvCountryCode = (TextView) findViewById(R.id.tvCountryCode);
        TextView tvLatitude = (TextView) findViewById(R.id.tvLatitude);
        TextView tvLongitude = (TextView) findViewById(R.id.tvLongitude);

        tvNome.setText(geoName.getName());
        tvCountryCode.setText(geoName.getCountrycode());
        tvLatitude.setText(geoName.getLat());
        tvLongitude.setText(geoName.getLng());

    }
}

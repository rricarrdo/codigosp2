package br.edu.ufabc.futuro.atividade5_geonames.View;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import br.edu.ufabc.futuro.atividade5_geonames.Controller.GeoNames_Fetcher;
import br.edu.ufabc.futuro.atividade5_geonames.Model.GeoName;
import br.edu.ufabc.futuro.atividade5_geonames.Model.GeoNamesDAO;
import br.edu.ufabc.futuro.atividade5_geonames.R;
import br.edu.ufabc.futuro.atividade5_geonames.Utils.NetworkUtils;


public class BuscaCidadesActivity extends ActionBarActivity {

    EditText etCoordenadaNorte;
    EditText etCoordenadaSul;
    EditText etCoordenadaLeste;
    EditText etCoordenadaOeste;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busca_cidades);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_busca_cidades, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void buscarCidades(View view) {

        etCoordenadaNorte = (EditText) findViewById(R.id.etCoordenadaNorte);
        etCoordenadaSul = (EditText) findViewById(R.id.etCoordenadaSul);
        etCoordenadaLeste = (EditText) findViewById(R.id.etCoordenadaLeste);
        etCoordenadaOeste = (EditText) findViewById(R.id.etCoordenadaOeste);

        if (NetworkUtils.isNetworkConnected(this)) {
            Toast.makeText(this, "Conectado!", Toast.LENGTH_SHORT).show();

            new GeoNames_FetchTask(this).execute(new GeoNames_Fetcher());
        }
    }


    private class GeoNames_FetchTask extends AsyncTask<GeoNames_Fetcher, Void, ArrayList<GeoName>> {
        Context context;

        private GeoNames_FetchTask(Context context) {
            this.context = context;
        }

        @Override
        protected ArrayList<GeoName> doInBackground(GeoNames_Fetcher... params) {

            String cNorte = etCoordenadaNorte.getText().toString();
            String cSul = etCoordenadaSul.getText().toString();
            String cLeste = etCoordenadaLeste.getText().toString();
            String cOeste = etCoordenadaOeste.getText().toString();
            return params[0].fetch(cNorte, cSul, cLeste, cOeste);
        }

        @Override
        protected void onPostExecute(ArrayList<GeoName> geonames) {
            GeoNamesDAO dao = GeoNamesDAO.newInstance(context);
            dao.geoNames = geonames;

            Intent intent = new Intent(context, ResultadoCidadesActivity.class);

            context.startActivity(intent);
        }
    }

}

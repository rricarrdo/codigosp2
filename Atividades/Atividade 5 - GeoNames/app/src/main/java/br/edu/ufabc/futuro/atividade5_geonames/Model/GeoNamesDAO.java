package br.edu.ufabc.futuro.atividade5_geonames.Model;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Ricardo on 06/04/2015.
 */
public class GeoNamesDAO {
    private static GeoNamesDAO dao;
    public ArrayList<GeoName> geoNames;
    private Context context;

    protected GeoNamesDAO(Context c) {
        geoNames = new ArrayList<GeoName>();
        context = c;
    }

    public static GeoNamesDAO newInstance(Context c) {
        if (dao == null) {
            dao = new GeoNamesDAO(c);
        } else
            dao.context = c;

        return dao;
    }

    public int size() {
        return geoNames.size();
    }

    public void add(GeoName task) {
        geoNames.add(task);
    }

    public GeoName getGeoNameAt(int position) {
        return geoNames.get(position);
    }
}

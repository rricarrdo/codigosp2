package br.edu.ufabc.futuro.atividade5_geonames.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import br.edu.ufabc.futuro.atividade5_geonames.Adapter.GeoNamesAdapter;
import br.edu.ufabc.futuro.atividade5_geonames.Model.GeoNamesDAO;
import br.edu.ufabc.futuro.atividade5_geonames.R;

public class ResultadoCidadesActivity extends ActionBarActivity {

    public static String POSITION_GEONAME = "positionGeoname";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado_cidades);

        GeoNamesDAO dao = GeoNamesDAO.newInstance(this);

        ListView lvNomesCidades = (ListView) findViewById(R.id.lvNomesCidades);
        GeoNamesAdapter adapter = new GeoNamesAdapter(this);
        lvNomesCidades.setAdapter(adapter);
        lvNomesCidades.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), DetalheCidadeActivity.class);
                intent.putExtra(POSITION_GEONAME, position);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_resultado_cidades, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

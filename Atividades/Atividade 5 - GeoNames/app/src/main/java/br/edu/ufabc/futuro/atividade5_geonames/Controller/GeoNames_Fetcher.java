package br.edu.ufabc.futuro.atividade5_geonames.Controller;

import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import br.edu.ufabc.futuro.atividade5_geonames.Model.GeoName;

/**
 * Created by Ricardo on 06/04/2015.
 */
public class GeoNames_Fetcher {

    private static final String ENDPOINT = "http://api.geonames.org/citiesJSON/";

    private static final String NORTH_K = "north";
    private static final String SOUTH_K = "south";
    private static final String EAST_K = "east";
    private static final String WEST_K = "west";
    private static final String LANGUAGE_K = "lang";
    private static final String USERNAME_K = "username";

    private static final String LANGUAGE_V = "en";
    private static final String USERNAME_V = "futuro";

    private static final String LOGTAG = GeoNames_Fetcher.class.getSimpleName();
    private static final int BUFFER_SIZE = 1024;


    public ArrayList<GeoName> fetch(String cNorte, String cSul, String cLeste, String cOeste) {
        ArrayList<GeoName> geoNames = new ArrayList<>();
        URL url;
        String urlStr = null;
        HttpURLConnection conn = null;
        int responseCode;

        try {
            urlStr = Uri.parse(ENDPOINT).buildUpon()
                    .appendQueryParameter(NORTH_K, cNorte)
                    .appendQueryParameter(SOUTH_K, cSul)
                    .appendQueryParameter(EAST_K, cLeste)
                    .appendQueryParameter(WEST_K, cOeste)
                    .appendQueryParameter(LANGUAGE_K, LANGUAGE_V)
                    .appendQueryParameter(USERNAME_K, USERNAME_V)
                    .build().toString();


            url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                InputStream inputStream = conn.getInputStream();
                int len;
                byte[] buffer = new byte[BUFFER_SIZE];

                while ((len = inputStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, len);
                }

                outputStream.flush();
                outputStream.close();
                inputStream.close();

                // deserialize JSON
                String jsonStr = new String(outputStream.toByteArray());
                geoNames.addAll(GeoName_Serializer.deserialize(jsonStr));
            }

        } catch (MalformedURLException e) {
            Log.e(LOGTAG, "Malformed URL", e);
        } catch (IOException e) {
            Log.e(LOGTAG, "Failed to open URL", e);
        }

        return geoNames;
    }
}
